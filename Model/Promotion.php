<?php

namespace Mavi\SalesRule\Model;

use \Magento\Framework\Model\AbstractModel;
use \Mavi\SalesRule\Api\Data\PromotionInterface;

class Promotion extends AbstractModel implements PromotionInterface
{
    const   CACHE_TAG = 'mavi_salesrule_promotion';
    /**
     * @var string
     */
    protected $_cacheTag = 'mavi_salesrule_promotion';
    /**
     * @var string
     */
    protected $_eventPrefix = 'mavi_salesrule_promotion';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('Mavi\SalesRule\Model\ResourceModel\Promotion');

    }

    /**
     * @api
     * @return string|null
     */
    public function getTriggerProduct()
    {
        return $this->getData(self::PROPERTY_TRIGGER_PRODUCT);
    }

    /**
     * @api
     * @param string $triggerProduct
     * @return $this
     */
    public function setTriggerProduct(string $triggerProduct)
    {
        return $this->setData(self::PROPERTY_TRIGGER_PRODUCT, $triggerProduct);
    }

    /**
     * @api
     * @return array|null
     */
    public function getCategoryProduct()
    {
        return $this->getData(self::PROPERTY_CATEGORY_PRODUCT);
    }

    /**
     * @api
     * @param array|string $categoryProduct
     * @return $this
     */
    public function setCategoryProduct(array|string $categoryProduct)
    {
        return $this->setData(self::PROPERTY_CATEGORY_PRODUCT, $categoryProduct);
    }
}