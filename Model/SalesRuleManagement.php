<?php

namespace Mavi\SalesRule\Model;

use Mavi\SalesRule\Api\SalesRuleManagementInterface;
use Mavi\SalesRule\Api\Data\PromotionInterfaceFactory;
use Mavi\SalesRule\Model\PromotionFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Framework\Webapi\Exception as WebapiException;

class SalesRuleManagement implements SalesRuleManagementInterface
{

    const XML_PATH = "sales_rule/";

    /**
     * @var PromotionInterfaceFactory
     */
    protected $_promotionInterfaceFactory;

    /**
     * @var PromotionFactory
     */
    protected $_promotionFactory;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param PromotionInterfaceFactory $promotionInterfaceFactory
     * @param PromotionFactory          $promotionFactory
     * @param ProductRepository         $productRepository
     * @param Request                   $request
     */
    public function __construct(
        PromotionInterfaceFactory $promotionInterfaceFactory,
        PromotionFactory          $promotionFactory,
        ProductRepository         $productRepository,
        StoreManagerInterface     $storeManager,
        Request                   $request
    )
    {
        $this->_promotionInterfaceFactory = $promotionInterfaceFactory;
        $this->_promotionFactory          = $promotionFactory;
        $this->productRepository          = $productRepository;
        $this->_storeManager              = $storeManager;
        $this->request                    = $request;
    }

    /**
     * Get promotion
     * @throws WebapiException
     */
    public function checkPromotionExists()
    {
        $body = $this->request->getBodyParams();

        $dataPromotion = $this->_promotionInterfaceFactory->create();
        $dataPromotion->setTriggerProduct($body['sku']);
        $dataPromotion->setCategoryProduct($body['category']);

        try {
            $collection = $this->_promotionFactory->create()->getCollection()
                ->addFieldToFilter('trigger_product', $dataPromotion->getTriggerProduct());
            $collection->getSelect()
                ->join(
                    ['sales_rule' => $collection->getTable('salesrule')],
                    'main_table.sales_rule_id = sales_rule.rule_id'
                )
                ->where('sales_rule.is_active = 1');

            $promotion = $collection->getFirstItem();

            // search by category
            if (empty($promotion->getData())) {
                $collection_reset = $promotion;
                $collection = $this->_promotionFactory->create()->getCollection()
                    ->addFieldToFilter('category_product', array('notnull' => true));
                $collection->getSelect()
                    ->join(
                        ['sales_rule' => $collection->getTable('salesrule')],
                        'main_table.sales_rule_id = sales_rule.rule_id'
                    )
                    ->where('sales_rule.is_active = 1');

                $containsSearch = 0;
                foreach ($collection as $promo) {
                    $categoryArray = array_map('trim', explode(',', $promo->getCategoryProduct()));
                    $containsSearch = count(array_intersect($categoryArray, $dataPromotion->getCategoryProduct()));

                    if ($containsSearch > 0) {
                        $promotion = $promo;
                        break;
                    }
                }

                if (!$containsSearch) $promotion = $collection_reset;

            }

            // search promotion by all
            if (empty($promotion->getData())) {
                $collection = $this->_promotionFactory->create()->getCollection()
                    ->addFieldToFilter('trigger_product', array('null' => true))
                    ->addFieldToFilter('category_product', array('null' => true));
                $collection->getSelect()
                    ->join(
                        ['sales_rule' => $collection->getTable('salesrule')],
                        'main_table.sales_rule_id = sales_rule.rule_id'
                    )
                    ->where('sales_rule.is_active = 1');

                $promotion = $collection->getFirstItem();
            }

            if (empty($promotion->getData())) {
                throw new WebapiException(__('Requested product doesn\'t exist'),
                    0, WebapiException::HTTP_NOT_FOUND
                );
            }

            $promotion?->setImage($this->getMediaUrl().$promotion->getImage());

            $product = null;
            if ($promotion?->getPromotionProduct()) {
                $product = $this->getProductBySku($promotion?->getPromotionProduct() ?? '');
            }

            return [
                "promotion" => $promotion->getData(),
                "product" => $product
            ];

        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
    }

    /**
     * Get product by SKU
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductBySku(string $sku)
    {
        $product = $this->productRepository->get($sku);
        return $product->getData();
    }

    /**
     * @return string media url
     */
    public function getMediaUrl()
    {
        $mediaUrl = $this->_storeManager->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'salesrule/tmp/image_banner/';
        return $mediaUrl;
    }

}