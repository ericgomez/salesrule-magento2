<?php

namespace Mavi\SalesRule\Model\ResourceModel\Promotion;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;


class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = '';
    /**
     * @var string
     */
    protected $_eventPrefix = 'mavi_salesrule_promotion_collection';
    /**
     * @var string
     */
    protected $_eventObject = 'promotion_collection';

    /**
     * Define resource model
     * @return void
    **/
    protected function _construct()
    {
        $this->_init('Mavi\SalesRule\Model\Promotion', 'Mavi\SalesRule\Model\ResourceModel\Promotion');
    }

}