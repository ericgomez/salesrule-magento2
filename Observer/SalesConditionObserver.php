<?php

namespace Mavi\SalesRule\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Mavi\SalesRule\Model\PromotionFactory;

/**
 * Class SalesConditionObserver
 */
class SalesConditionObserver implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    protected $_promotionFactory;

    private   $_serializer;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param SerializerInterface                       $serializer
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface   $objectManager,
        SerializerInterface                         $serializer,
        PromotionFactory                            $promotionFactory

    ) {
        $this->_objectManager =     $objectManager;
        $this->_serializer    =     $serializer;
        $this->_promotionFactory =  $promotionFactory;
    }

    /**
     * customer register event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $data    = $observer->getRequest()->getPostValue();
        $rule_id = $observer->getEvent()->getRequest()->getParam('rule_id');
        
        $conditionsUnserialize = [];
        if(isset($data['conditions_serialized'])) $conditionsUnserialize = $this->_serializer->unserialize($data['conditions_serialized']);
        else if(isset($data['rule']['conditions'])) $conditionsUnserialize = $data['rule']['conditions'];

        $actionsUnserialize = [];
        if(isset($data['actions_serialized'])) $actionsUnserialize = $this->_serializer->unserialize($data['actions_serialized']);
        else if(isset($data['rule']['actions'])) $actionsUnserialize = $data['rule']['actions'];

        $skuPromo    = $this->array_search_id('sku', $actionsUnserialize) ?? '';
        $skuTrigger  = $this->array_search_id('sku', $conditionsUnserialize) ?? '';
        $categoryIds = $this->array_search_id('category_ids', $conditionsUnserialize) ?? '';
        $quoteItemPrice = $this->array_search_id('quote_item_price', $conditionsUnserialize);
        $qty = $this->array_search_id('qty', $conditionsUnserialize);

        $model = $this->_promotionFactory->create()->getCollection()
            ->addFieldToFilter('sales_rule_id', $rule_id)
            ->getFirstItem();

        if(isset($data['rule_id'])) {
            $model->setSalesRuleId($data['rule_id']);
            $model->setDescription($data['name']);
            $model->setPromotionProduct($skuPromo);
            $model->setTriggerProduct($skuTrigger);
            $model->setCategoryProduct($categoryIds);
            $model->setMinimumAmount($quoteItemPrice);
            $model->setRequiredQty($qty);

            $model->save();
        }
    }
    
    // Function to recursively search for a given value
    public function array_search_id($attribute, $array) {

        if(is_array($array) && count($array) > 0) {
            $temp_value = '';
            foreach($array as $key => $value) {

                if($value == $attribute) {
                    $temp_value = $value;
                } else if(is_array($value) && count($value) > 0) {
                    $res_path = $this->array_search_id($attribute, $value);
                    if ($res_path != null) return $res_path;
                }

                if($temp_value) {
                    if($key == 'value') return $value;
                }
            }
        }

        return null;
    }
}