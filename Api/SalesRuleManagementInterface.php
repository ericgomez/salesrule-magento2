<?php

namespace Mavi\SalesRule\Api;

interface SalesRuleManagementInterface
{
    /**
     * @return Mavi\SalesRule\Api\Data\PromotionInterface[]|array
     */
    public function checkPromotionExists();
}