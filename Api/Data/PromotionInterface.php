<?php

namespace Mavi\SalesRule\Api\Data;

interface PromotionInterface {
    const PROPERTY_TRIGGER_PRODUCT  = 'trigger_product';
    const PROPERTY_CATEGORY_PRODUCT = 'category_product';

    /**
     * @return string|null
     */
    public function getTriggerProduct();

    /**
     * @param string $triggerProduct
     * @return $this
     */
    public function setTriggerProduct(string $triggerProduct);

    /**
     * @return array|string|null
     */
    public function getCategoryProduct();

    /**
     * @param array|string $categoryProduct
     * @return $this
     */
    public function setCategoryProduct(array|string $categoryProduct);
}