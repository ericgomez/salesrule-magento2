<?php

namespace Mavi\SalesRule\Plugin\Banner;

/**
 * Class DataProvider
 */
class DataProvider
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    protected $storeManager;
    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function afterGetData(
        \Magento\SalesRule\Model\Rule\DataProvider $subject,
        $loadedData
    ) {
        /** @var array $loadedData */
        if (is_array($loadedData) && count($loadedData) == 1) {
            foreach ($loadedData as $key => $item) {
                if (isset($item['image']) && $item['image']) {
                    $imageArr = [];
                    $imageArr[0]['name'] = $item['image'];
                    $imageArr[0]['url'] = $this->getMediaUrl() . $item['image'];
                    $loadedData[$key]['image'] = $imageArr;
                }
            }
        }
        return $loadedData;
    }

    /**
     * @return string media url
     */
    public function getMediaUrl()
    {
        $mediaUrl = $this->_storeManager->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'salesrule/tmp/image_banner/';
        return $mediaUrl;
    }
}